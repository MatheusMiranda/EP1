#include "imagem.hpp"
#include "filtroprincipal.hpp"
#include "negativo.hpp"
#include "sharpen.hpp"
#include "smooth.hpp"


using namespace std;

int main(){

	Imagem imagemLena;
	sharpen opcaosharpen;
	negativo opcaonegativo;
	smooth opcaosmooth;
	int escolhadeefeito;
	imagemLena.abrirImagem("../lena512.pgm");
	
	cout << "Qual dos seguintes efeitos deseja aplicar na imagem: Sharpen(digite 1), Negativo(digite 2) ou smooth(digite 3)" << endl;

	cin >> escolhadeefeito;

	if(escolhadeefeito == 1) {

		opcaosharpen.setDiv(1);
		opcaosharpen.setSize(3);
		opcaosharpen.efeito(imagemLena, opcaosharpen.getDiv(), opcaosharpen.getSize());
		imagemLena.salvarImagem("../lenaSharpen.pgm");
	}

	if(escolhadeefeito == 2) {

		opcaonegativo.efeito(imagemLena);
		imagemLena.salvarImagem("../lenaNegativo.pgm");

	}
	if(escolhadeefeito == 3) {

		opcaosmooth.setDiv(9);
		opcaosmooth.setSize(3);
		opcaosmooth.efeito(imagemLena,opcaosmooth.getDiv(), opcaosmooth.getSize());
		imagemLena.salvarImagem("../lenaSmooth.pgm");
	
	}
	else{

		exit(1);
	}
	
		
	return 0;
}

