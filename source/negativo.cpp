#include "negativo.hpp"
#include "filtroprincipal.hpp"

using namespace std;

void negativo::efeito(Imagem &imagemLena) {
		
		int linhas, colunas;
		
		linhas = imagemLena.getLinhas();
		colunas = imagemLena.getColunas();
		
		for (int i=0; i < linhas; i++){
			for (int j = 0; j < colunas; j++) {
				imagemLena.setPixel(i,j, 255-imagemLena.getPixel(i, j) );
			}
		} 


}