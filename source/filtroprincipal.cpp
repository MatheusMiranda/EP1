#include "filtroprincipal.hpp"

using namespace std;


filtroprincipal::filtroprincipal(){
	
	div = 0;
	size = 0;

}
filtroprincipal::filtroprincipal(int div, int size){
	
	this->div = div;
	this->size = size;

}

int filtroprincipal::getDiv(){
	
	return div;

}

int filtroprincipal::getSize(){
	
	return size;

}

void filtroprincipal::setDiv(int div){
	
	this->div = div;

}


void filtroprincipal::setSize(int size){
	
	this->size = size;

}

