#include "imagem.hpp"

using namespace std;
Imagem::Imagem() {
	linhas = 0;
	colunas = 0;
	niveisdecinza = 0;
	matriz = NULL;
}

void Imagem::abrirImagem(const char fname[]){

	int i, j;
	char header [100], *ptr;
	unsigned char *charImage;
    ifstream ifp;//ponteiro para o arquivo
	ifp.open(fname, ios::in | ios::binary);
	
    if (!ifp) 
    {
        cout << "Não foi possível ler a imagem: " << fname << endl;
       exit(1);//encerra o programa caso não seja possível ler a imagem
    }

    //Este trecho do código verifica se o arquivo referenciado é do formato PGM, caso seja uma imagem PGM, a mesma será identificada por um número mágico.
    ifp.getline(header,100,'\n');
    if ( (header[0]!='P') || (header[1]!='5') )
    {   
        cout << "Imagem " << fname << " Não é um arquivo PGM" << endl;
        exit(1);//encerra o programa caso não seja uma imagem PGM
    }
    //ignorar os comentários, iniciados por #
    ifp.getline(header,100,'\n');
    while(header[0]=='#')
        ifp.getline(header,100,'\n');
    
    linhas=strtol(header,&ptr,0);
    colunas=atoi(ptr);
    //conversão para inteiro
    ifp.getline(header,100,'\n');
    niveisdecinza=strtol(header,&ptr,0);
    matriz = new int[linhas*colunas];

    charImage = (unsigned char *) new unsigned char [linhas*colunas];

    ifp.read( reinterpret_cast<char *>(charImage), (linhas*colunas)*sizeof(unsigned char));

    if (ifp.fail()) 
    {
        cout << "Imagem " << fname << " é do tamanho errado" << endl;
        exit(1);
    }

    ifp.close();

    int val;

    for(i=0; i<linhas; i++){
        for(j=0; j<colunas; j++) 
        {
            val = (int)charImage[i*colunas+j];
            matriz[i*colunas+j] = val;   
        }
        
     
}
     delete [] charImage;


}
Imagem::Imagem(int linhas, int colunas, int niveisdecinza) {
    this->linhas = linhas;
    this->colunas = colunas;
    this->niveisdecinza = niveisdecinza;
    //Criando um vetor de inteiros com o tamanho da imagem para poder trabalhar com cada pixel
    matriz = new int[linhas*colunas];
}

Imagem::~Imagem(){
	delete []matriz;
}

int Imagem::getLinhas() {
	return linhas;	
}

int Imagem::getColunas() {
	return colunas;
}

int Imagem::getniveisdeCinza() {
	return niveisdecinza;
}

void Imagem::setLinhas(int linhas) {
	this->linhas=linhas;	
}

void Imagem::setColunas(int colunas) {
	this->colunas = colunas;
}

void Imagem::setniveisdeCinza(int niveisdecinza) {
	this->niveisdecinza = niveisdecinza;
}



 void Imagem::salvarImagem(const char filename[]) {
	int i, j;
	unsigned char *charImage;
	ofstream outfile(filename);
	
	charImage = (unsigned char *) new unsigned char [linhas*colunas];
	
	int val;

    for(i=0; i<linhas; i++)
    {
        for(j=0; j<colunas; j++) 
        {
			val = matriz[i*colunas+j];
			charImage[i*colunas+j]=(unsigned char)val;
        }
    }
    
	if (!outfile.is_open())
	{
		cout << "Não foi possível abrir o arquivo de saída"  << filename << endl;
		exit(1);//encerra o programa
	}
	
	outfile << "P5" << endl;
    outfile << linhas << " " << colunas << endl;
    outfile << 255 << endl;
	
	outfile.write(reinterpret_cast<char *>(charImage), (linhas*colunas)*sizeof(unsigned char));
	cout << "Arquivo criado com sucesso" << endl;
	outfile.close();
}
//Os dois métodos abaixo possuem a funcionalidade de acessar o valor de um determinado elemento da "matriz" em uma posição espexífica, ou seja, um pixel da imagem.
int Imagem::getPixel(int linha, int coluna)
{
	int sizeY = getColunas();
    return matriz[linha*sizeY+coluna];
}

void Imagem::setPixel(int linha, int coluna, int valor)
{
	int sizeY = getColunas();
	matriz[linha*sizeY+coluna] = valor;
}

