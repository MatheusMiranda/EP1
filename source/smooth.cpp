#include "smooth.hpp"
#include "filtroprincipal.hpp"

using namespace std;

void smooth::efeito(Imagem &imagemLena, int div, int size) {
	
		int smooth[] = {1, 1, 1, 1, 1, 1, 1, 1, 1};
		int linhas, colunas, valor, i,j;
		linhas = imagemLena.getLinhas();
		colunas = imagemLena.getColunas();
		int *m = new int[linhas*colunas];
		
		for (i=size/2; i < linhas-size/2; i++)
		{
			for (j = size/2; j < colunas-size/2; j++)
			{
				valor = 0;
				for(int x = -1; x<=1; x++)
				{		
					for(int y = -1; y<=1; y++)
					{		
						valor += smooth[(x+1)+ size*(y+1)] *
					    imagemLena.getPixel(i+x, y+j);						
					}
				}
				
				valor /=div;
				
			
					
				valor= valor < 0 ? 0 : valor;
				valor=valor >255 ? 255 : valor;
				
				m[i+colunas*j] = valor;
			}
		}
		
		for(i=0;i<linhas; i++)
			for(j=0; j<colunas; j++)
				imagemLena.setPixel(i, j, m[i+colunas*j]);
				

}
