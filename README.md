Aluno: Matheus Miranda Lacerda – 14/0056793

Como utilizar o programa:

*Utilizando o terminal, entre no diretório principal onde estão localizados os diretórios bin, doc, include, obj, source, e a imagem.
*No terminal, digite o comando make. Este comando deverá compilar perfeitamente os códigos.
*Pelo terminal, entre na pasta bin(onde ficará o executável criado pelo comando make).
*No terminal digite o comando ./finalBinary. Esse comando executará o binário.
*Digite a opção de filtro que deseja aplicar sobre a imagem. 
*Pronto!! A imagem com o filtro aplicado será criada no diretório principal.

OBS: Para que o programa funcione corretamente, a imagem deve estar no diretório principal, juntamente com os diretórios bin, doc, include, obj, source. Caso queira aplicar filtros na imagem em outro lugar, basta ir no arquivo main.cpp e alterar o caminho da imagem.
