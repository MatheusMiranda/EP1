#ifndef IMAGEM_H
#define IMAGEM_H
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

using namespace std;

class Imagem{
private:
	int linhas;
	int colunas;
	int niveisdecinza;
	int *matriz;//vetor que trabalhará com os pixels da imagem
public:
	Imagem();
	Imagem(int linhas, int colunas, int niveisdecinza);
	void abrirImagem(const char fname[]);	
	void salvarImagem(const char filename[]);	
	int getLinhas();
	int	getColunas();
	int getniveisdeCinza();
	void setLinhas(int linhas);
	void setColunas(int colunas);
	void setniveisdeCinza(int niveisdecinza);	
	int getPixel(int linha, int coluna);
	void setPixel(int linha, int coluna, int valor);	
	~Imagem();//desconstrutor da classe
};
#endif
