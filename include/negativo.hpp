#ifndef NEGATIVO_H
#define NEGATIVO_H
#include "imagem.hpp"
#include "filtroprincipal.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 


using namespace std;

class negativo : public filtroprincipal {
	public:
		void efeito(Imagem &imagemLena);		
};

#endif
