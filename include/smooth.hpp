#ifndef SMOOTH_H
#define SMOOTH_H
#include "imagem.hpp"
#include "filtroprincipal.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

using namespace std;

class smooth : public filtroprincipal{
	public:
		void efeito(Imagem &imagemLena,int div,int size);

};
#endif