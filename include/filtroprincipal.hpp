#ifndef FILTROPRINCIPAL_h
#define FILTROPRINCIPAL_h

using namespace std;

#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h> 
#include <sstream>

using namespace std;

class filtroprincipal {
	private:
		//estas duas variáveis já estão presentes nos códigos dos filtros smooth e sharpen;
		int div;
		int size;
		//eu poderia declará-las em cada uma das classe, mas desse modo é possível utilizar o conceito de herança
	public:
		filtroprincipal();
		filtroprincipal(int div, int size);
		int getDiv();
		int getSize();
		void setDiv(int div);
		void setSize(int size);

};
#endif
