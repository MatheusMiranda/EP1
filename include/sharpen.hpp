#ifndef SHARPEN_H
#define SHARPEN_H
#include "imagem.hpp"
#include "filtroprincipal.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

using namespace std;

class sharpen : public filtroprincipal{
	public:
		void efeito(Imagem &imagemLena,int div, int size);

};

#endif